#Download local dynamodb zip file
echo "Download dynamodb file"
wget  http://dynamodb-local.s3-website-us-west-2.amazonaws.com/dynamodb_local_latest.zip

#get current path of the folder
myvar=$(pwd)


#unzip the dynamo db file
echo "unzip dynamo db file"
if which unzip >/dev/null;
then
    echo exists
    unzip dynamodb_local_latest.zip
else
    echo does not exist
    apt-get install unzip
    unzip dynamodb_local_latest.zip
fi

#Run dynamodb in system
echo "Run local dynamo db"
java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb &

#Install aws console
echo "Install aws console"
apt-get install awscli

#aws configure
echo "Setup aws configure"
cd 
mkdir .aws
touch ~/.aws/config
cd "$myvar"
cp awsconfigure  ~/.aws/config


#call Dynamo db table script file
echo "Running dynamo db table script"
sh ./dynamoDbTableScript.sh



