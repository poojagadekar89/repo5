#********* TO create AgentActivity table **************

aws dynamodb create-table \
    --table-name AgentActivity \
    --attribute-definitions \
        AttributeName=pSid,AttributeType=S AttributeName=tsd,AttributeType=S \
    --key-schema AttributeName=pSid,KeyType=HASH AttributeName=tsd,KeyType=RANGE \
    --provisioned-throughput ReadCapacityUnits=40,WriteCapacityUnits=20 --endpoint=http://localhost:8000

#*********** To create AgentTrenData table *******************

aws dynamodb create-table \
    --table-name AgentTrendData \
    --attribute-definitions \
        AttributeName=pSid,AttributeType=S AttributeName=tsd,AttributeType=S \
    --key-schema AttributeName=pSid,KeyType=HASH AttributeName=tsd,KeyType=RANGE \
    --provisioned-throughput ReadCapacityUnits=80,WriteCapacityUnits=40 --endpoint=http://localhost:8000

#*************** To create CallEndLogNew Table ************

      aws dynamodb create-table \
    --table-name CallEndLogNew \
    --attribute-definitions \
        AttributeName=CallLegSid,AttributeType=S AttributeName=VirtualCallSid,AttributeType=S \
    --key-schema AttributeName=CallLegSid,KeyType=HASH AttributeName=VirtualCallSid,KeyType=RANGE \
    --global-secondary-index '[
    {
        "IndexName": "VirtualCallSid",
        "KeySchema": [
            {
                "AttributeName": "VirtualCallSid",
                "KeyType": "HASH"
            }
        ],
        "Projection": {
            "ProjectionType": "ALL"
        },
        "ProvisionedThroughput": {
            "ReadCapacityUnits": 2,
            "WriteCapacityUnits": 1
        }
    }
]' \
--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=3 --endpoint=http://localhost:8000 


#************ To create CallTextMessageNew Table ***********

aws dynamodb create-table \
    --table-name CallTextMessageNew \
    --attribute-definitions \
        AttributeName=ChatSessionId,AttributeType=S AttributeName=CustomerSid,AttributeType=S AttributeName=CreatedDate,AttributeType=S  \
    --key-schema AttributeName=ChatSessionId,KeyType=HASH AttributeName=CustomerSid,KeyType=RANGE \
    --global-secondary-index '[
    {
        "IndexName": "CustomerSidIndex",
        "KeySchema": [
            {
                "AttributeName": "CustomerSid",
                "KeyType": "HASH"
            },
		{
                "AttributeName": "CreatedDate",
                "KeyType": "RANGE"
            }
        ],
        "Projection": {
        "ProjectionType":"INCLUDE",
           "NonKeyAttributes": ["ChatSessionId","CustomerSid","CreatedDate","JsonMessage"]
        },
        "ProvisionedThroughput": {
            "ReadCapacityUnits": 2,
            "WriteCapacityUnits": 1
        }
    }
]' \
--provisioned-throughput ReadCapacityUnits=3,WriteCapacityUnits=2  --endpoint=http://localhost:8000


#********************* To create CaseChangeEntryNew ***************/

aws dynamodb create-table \
    --table-name CaseChangeEntryNew \
    --attribute-definitions \
        AttributeName=ID,AttributeType=S AttributeName=CaseEventId,AttributeType=S \
    --key-schema AttributeName=ID,KeyType=HASH  \
    --global-secondary-index '[
    {
        "IndexName": "CaseEventId-index",
        "KeySchema": [
            {
                "AttributeName": "CaseEventId",
                "KeyType": "HASH"
            }
        ],
        "Projection": {
        "ProjectionType":"ALL"
           
        },
        "ProvisionedThroughput": {
            "ReadCapacityUnits": 10,
            "WriteCapacityUnits": 5
        }
    }
]' \
--provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --endpoint=http://localhost:8000

#**************** To Create CaseConversationAssociation ****************/

aws dynamodb create-table \
    --table-name CaseConversationAssociation \
    --attribute-definitions \
        AttributeName=ConvrId,AttributeType=S \
    --key-schema AttributeName=ConvrId,KeyType=HASH  \
--provisioned-throughput ReadCapacityUnits=20,WriteCapacityUnits=10 --endpoint=http://localhost:8000

#************** TO create CaseConversationContentNew **************/

aws dynamodb create-table \
    --table-name CaseConversationContentNew \
    --attribute-definitions \
        AttributeName=CaseId,AttributeType=S AttributeName=Timestamp,AttributeType=S AttributeName=ConvrId,AttributeType=S \
    --key-schema AttributeName=CaseId,KeyType=HASH AttributeName=Timestamp,KeyType=RANGE  \
    --global-secondary-index '[
    {
        "IndexName": "ConvrIdTimestampIndex",
        "KeySchema": [
            {
                "AttributeName": "ConvrId",
                "KeyType": "HASH"
            },
{
                "AttributeName": "Timestamp",
                "KeyType": "RANGE"
            }

        ],
        "Projection": {
        "ProjectionType":"ALL"
           
        },
        "ProvisionedThroughput": {
            "ReadCapacityUnits": 1,
            "WriteCapacityUnits": 1
        }
    }
]' \
--provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 --endpoint=http://localhost:8000

#************** To create CaseConversationNew table *****************/

aws dynamodb create-table \
    --table-name CaseConversationNew \
    --attribute-definitions \
        AttributeName=CaseId,AttributeType=S AttributeName=Timestamp,AttributeType=S AttributeName=UserId,AttributeType=S \
    --key-schema AttributeName=CaseId,KeyType=HASH AttributeName=Timestamp,KeyType=RANGE  \
    --local-secondary-index '[
    {
        "IndexName": "UserIdIndex",
        "KeySchema": [
            {
                "AttributeName": "CaseId",
                "KeyType": "HASH"
            },
{
                "AttributeName": "UserId",
                "KeyType": "RANGE"
            }

        ],
        "Projection": {
        "ProjectionType":"ALL"
           
        }
    }
]' \
--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=6 --endpoint=http://localhost:8000

#************** To create CaseConversationThread *************/

aws dynamodb create-table \
    --table-name CaseConversationThread \
    --attribute-definitions \
        AttributeName=sid,AttributeType=S \
    --key-schema AttributeName=sid,KeyType=HASH \
--provisioned-throughput ReadCapacityUnits=20,WriteCapacityUnits=10 
/**********To create CaseEventNew *************
aws dynamodb create-table \
    --table-name CaseEventNew \
    --attribute-definitions \
        AttributeName=EventId,AttributeType=S AttributeName=CaseId,AttributeType=S AttributeName=TimeStamp,AttributeType=S \
    --key-schema AttributeName=EventId,KeyType=HASH \
    --global-secondary-index '[
    {
        "IndexName": "CaseId-TimeStamp-index",
        "KeySchema": [
            {
                "AttributeName": "CaseId",
                "KeyType": "HASH"
            },
{
                "AttributeName": "TimeStamp",
                "KeyType": "RANGE"
            }

        ],
        "Projection": {
        "ProjectionType":"ALL"
           
        },
 "ProvisionedThroughput": {
            "ReadCapacityUnits": 10,
            "WriteCapacityUnits": 5
        }
    }
]' \
--provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --endpoint=http://localhost:8000

#************ To create ChatConversation *************/

 aws dynamodb create-table \
    --table-name ChatConversation \
    --attribute-definitions \
        AttributeName=ProjId,AttributeType=S AttributeName=Id,AttributeType=S \
    --key-schema AttributeName=ProjId,KeyType=HASH AttributeName=Id,KeyType=RANGE \
--provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5  --endpoint=http://localhost:8000
aws dynamodb list-tables 

#*********** To create ChatMessage **************/

aws dynamodb create-table \
    --table-name ChatMessage \
    --attribute-definitions \
        AttributeName=ConvrId,AttributeType=S AttributeName=Timestamp,AttributeType=S \
    --key-schema AttributeName=ConvrId,KeyType=HASH AttributeName=Timestamp,KeyType=RANGE \
--provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --endpoint=http://localhost:8000

#****** To create ProjectCaseTag ************/
aws dynamodb create-table \
    --table-name ProjectCaseTag \
    --attribute-definitions \
        AttributeName=ProjectId,AttributeType=S AttributeName=Tag,AttributeType=S \
    --key-schema AttributeName=ProjectId,KeyType=HASH AttributeName=Tag,KeyType=RANGE \
--provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --endpoint=http://localhost:8000


#************** To create ChatConversationNew table *****************/

aws dynamodb create-table \
    --table-name ChatConversationNew \
    --attribute-definitions \
        AttributeName=ProjSid,AttributeType=S AttributeName=ConvrId,AttributeType=S AttributeName=Timestamp,AttributeType=S AttributeName=CustSid,AttributeType=S AttributeName=Status,AttributeType=S \
    --key-schema AttributeName=ProjSid,KeyType=HASH AttributeName=ConvrId,KeyType=RANGE  \
    --local-secondary-index '[
    {
        "IndexName": "TstIdx",
        "KeySchema": [
            {
                "AttributeName": "ProjSid",
                "KeyType": "HASH"
            },
			{
                "AttributeName": "Timestamp",
                "KeyType": "RANGE"
            }

        ],
        "Projection": {
        "ProjectionType":"ALL"
           
        }
    }
    ,
    {
        "IndexName": "CustSidIdx",
        "KeySchema": [
            {
                "AttributeName": "ProjSid",
                "KeyType": "HASH"
            },
			{
                "AttributeName": "CustSid",
                "KeyType": "RANGE"
            }

        ],
        "Projection": {
        "ProjectionType":"ALL"
           
        }
    },
    {
        "IndexName": "StatusIdx",
        "KeySchema": [
            {
                "AttributeName": "ProjSid",
                "KeyType": "HASH"
            },
			{
                "AttributeName": "Status",
                "KeyType": "RANGE"
            }

        ],
        "Projection": {
        "ProjectionType":"ALL"
           
        }
    }
]' \
--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=6 --endpoint=http://localhost:8000


#************************* TO Create ChatDeliveredMessages table ****************************/
aws dynamodb create-table \
    --table-name ChatDeliveredMessages \
    --attribute-definitions \
        AttributeName=ChatMesgId,AttributeType=S AttributeName=Timestamp,AttributeType=S \
    --key-schema AttributeName=ChatMesgId,KeyType=HASH  \
    --global-secondary-index '[
    {
        "IndexName": "ChatMesgId-Timestamp-index",
        "KeySchema": [
            {
                        "KeyType": "HASH", 
                        "AttributeName": "ChatMesgId"
                    }, 
                    {
                        "KeyType": "RANGE", 
                        "AttributeName": "Timestamp"
                    }

        ],
        "Projection": {
        "ProjectionType":"ALL"
           
        },
        "ProvisionedThroughput": {
            "ReadCapacityUnits": 5,
            "WriteCapacityUnits": 3
        }
    }
]' \
--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=3 --endpoint=http://localhost:8000

#************************* TO Create ChatMessageReadBy table ****************************/
aws dynamodb create-table \
    --table-name ChatMessageReadBy \
    --attribute-definitions \
        AttributeName=ChatMesgId,AttributeType=S AttributeName=Timestamp,AttributeType=S \
    --key-schema AttributeName=ChatMesgId,KeyType=HASH  \
    --global-secondary-index '[
    {
        "IndexName": "ChatMesgId-Timestamp-index",
        "KeySchema": [
           {
                        "KeyType": "HASH", 
                        "AttributeName": "ChatMesgId"
                    }, 
                    {
                        "KeyType": "RANGE", 
                        "AttributeName": "Timestamp"
                    }
        ],
        "Projection": {
        "ProjectionType":"ALL"
           
        },
        "ProvisionedThroughput": {
            "ReadCapacityUnits": 5,
            "WriteCapacityUnits": 3
        }
    }
]' \
--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=3 --endpoint=http://localhost:8000
